import React, {Component} from "react";
import {Table, Button, Popconfirm, Icon, Upload} from "antd";
import {ExcelRenderer} from "react-excel-renderer";
import {EditableFormRow, EditableCell} from "../utils/editable";

export default class ExcelPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cols: [],
            rows: [],
            errorMessage: null,
            columns: [
                {
                    title: "Full name",
                    dataIndex: "full_name",
                    editable: true
                },
                {
                    title: "Phone",
                    dataIndex: "phone",
                    editable: true
                },
                {
                    title: "Email",
                    dataIndex: "email",
                    editable: true
                },
                {
                    title: "Age",
                    dataIndex: "age",
                    editable: true
                },
                {
                    title: "Experience",
                    dataIndex: "experience",
                    editable: true
                },
                {
                    title: "Yearly Income",
                    dataIndex: "yearly_income",
                    editable: true
                },
                {
                    title: "Has children",
                    dataIndex: "has_children",
                    editable: true
                },
                {
                    title: "License states",
                    dataIndex: "license_states",
                    editable: true
                },
                {
                    title: "Expiration date",
                    dataIndex: "expiration_date",
                    editable: true
                },
                {
                    title: "License number",
                    dataIndex: "license_number",
                    editable: true
                },
                {
                    title: "Action",
                    dataIndex: "action",
                    render: (text, record) =>
                        this.state.rows.length >= 1 ? (
                            <Popconfirm
                                title="Sure to delete?"
                                onConfirm={() => this.handleDelete(record.key)}
                            >
                                <Icon
                                    type="delete"
                                    theme="filled"
                                    style={{color: "red", fontSize: "20px"}}
                                />
                            </Popconfirm>
                        ) : null
                }
            ]
        };
    }
    handleSave = (row) => {
        const newData = [...this.state.rows];
        const index = newData.findIndex((item) => row.key === item.key);
        const item = newData[index];
        newData.splice(index, 1, {
            ...item,
            ...row
        });
        this.setState({rows: newData});
    };
    checkFile(file) {
        let errorMessage = "";
        if (!file || !file[0]) {
            return;
        }
        const isExcel =
            file[0].type === "application/vnd.ms-excel" ||
            file[0].type ===
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        if (!isExcel) {
            errorMessage = "You can only upload Excel file!";
        }
        const isLt2M = file[0].size / 1024 / 1024 < 2;
        if (!isLt2M) {
            errorMessage = "File must be smaller than 2MB!";
        }
        return errorMessage;
    }
    fileHandler = (fileList) => {
        let fileObj = fileList;
        if (!fileObj) {
            this.setState({
                errorMessage: "No file uploaded!"
            });
            return false;
        }
        if (
            !(
                fileObj.type === "application/vnd.ms-excel" ||
                fileObj.type ===
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            )
        ) {
            this.setState({
                errorMessage: "Unknown file format. Only Excel files are uploaded!"
            });
            return false;
        }

        function isDecimal(n) {
            if (n === "")
                return false;
            var strCheck = "0123456789";
            var i;

            for (i in n) {
                if (strCheck.indexOf(n[i]) === -1)
                    return false;
            }
            return true;
        }

        function telephoneCheck(str) {
            if (!str) {

                alert('\n' +
                    'No entry is correct')
                return window.location.reload()
            } else {
                var isphone = /^(1\s|1|)?((\(\d{3}\))|\d{3})(-|\s)?(\d{3})(-|\s)?(\d{4})$/.test(str);
                console.log(str);
                if (isphone === true) {
                    if (str[0] === "1") {
                        return "+" + str
                    }
                    if (str[0] === "+") {
                        return str
                    }
                    if (str[0] !== "+" && str[0] !== "1") {
                        return '+1' + str
                    }
                }
            }

        }

        function fullName(name) {
            if (name) {
                return name.toString().trim()
            } else {
                alert('\n' +
                    'No entry is correct')
                return window.location.reload()
            }
        }

        function email(email) {
            if (email) {
                return (email.toString().toLowerCase().trim())
            } else {
                alert('\n' +
                    'No entry is correct')
                return window.location.reload()
            }
        }

        function formattedDate(d = new Date()) {
            let month = String(d.getMonth() + 1);
            let day = String(d.getDate());
            const year = String(d.getFullYear());

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return `${day}/${month}/${year}`;
        }

        ExcelRenderer(fileObj, (err, resp) => {
            if (err) {
                throw err
            } else {
                let newRows = [];
                //eslint-disable-next-line
                resp.rows.slice(1).map((row, index) => {
                    if (row && row !== "undefined" && row !== "NaN") {
                        newRows.push({
                            key: index,
                            full_name: fullName(row[0]),
                            phone: telephoneCheck(row[1]),
                            email: email(row[2]),
                            age: row[3] >= 21 ? row[3] : null,
                            experience: row[4] >= 0 && row[4] < row[3] ? row[4] : null,
                            yearly_income: Number.isInteger(row[5]) && isDecimal(row[5]) && row[5] < 10000000 ? row[5] : null,
                            has_children: !row[6] ? 'FALSE' : row[6],
                            license_states: row[7],
                            expiration_date: formattedDate(),
                            license_number: row[9],
                        });
                    }
                });

                if (newRows.length === 0) {
                    this.setState({
                        errorMessage: "No data found in file!"
                    });
                    return false;
                } else {
                    this.setState({
                        cols: resp.cols,
                        rows: newRows,
                        errorMessage: null
                    });
                }
            }
        });
        return false;
    };


    handleDelete = (key) => {
        const rows = [...this.state.rows];
        this.setState({rows: rows.filter((item) => item.key !== key)});
    };
    handleAdd = () => {
        const {count, rows} = this.state;
        const newData = {
            key: count,
            full_name: '',
            phone: '',
            email: '',
            age: '',
            experience: '',
            yearly_income: '',
            has_children: '',
            license_states: '',
            expiration_date: '',
            license_number: '',

        };
        this.setState({
            rows: [newData, ...rows],
            count: count + 1
        });
    };
    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell
            }
        };
        const columns = this.state.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: (record) => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    handleSave: this.handleSave
                })

            };
        });
        return (
            <>
                <div>
                    <div style={{margin: "0 auto"}}>
                        <Table
                            style={{margin: "0 auto"}}
                            components={components}
                            rowClassName={() => "editable-row"}
                            dataSource={this.state.rows}
                            columns={columns}
                        />
                        <div style={{margin: "0 auto", width: "max-content"}}>
                            <Upload
                                name="file"
                                beforeUpload={this.fileHandler}
                                onRemove={() => this.setState({rows: []})}
                                multiple={false}
                            >
                                <Button
                                    size={'large'}
                                    styles={{minWidth: '180px'}}>
                                    <Icon type="upload"/> Import users
                                </Button>

                            </Upload>
                            <a

                                href="https://res.cloudinary.com/bryta/raw/upload/v1562751445/Users.xlsx"
                                target="_blank"
                                rel="noopener noreferrer"
                                download
                            >
                                <Button
                                    size={'large'}
                                    style={{minWidth: '180px'}}>
                                    Download to device
                                </Button>
                            </a>
                            {this.state.rows.length > 0 && (
                                <>
                                    <Button
                                        styles={{minWidth: '180px'}}
                                        onClick={this.handleAdd}
                                        size="large"
                                        type="info"
                                    >
                                        <Icon type="plus"/>
                                        Add row
                                    </Button>{" "}
                                </>
                            )}
                        </div>

                    </div>

                </div>
            </>
        );
    }
}
