import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import ExcelPage from "./components/excelPage";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";

function App() {
  return (
      <>
          <ErrorBoundary>
              <ExcelPage/>
          </ErrorBoundary>
      </>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
